#!/bin/sh

if [ ! -e /vol/wiki/tiddlywiki.info ]; then
  tiddlywiki /vol/wiki --init server
fi

tiddlywiki /vol/wiki --listen host=0.0.0.0
