FROM node:current-alpine3.13

COPY bin/ /opt/bin/

VOLUME /vol/wiki

RUN npm install -g tiddlywiki
CMD /opt/bin/entrypoint.sh
